<?php

/**
 * @file
 * uw_ct_feds_employ_volunteer.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_feds_employ_volunteer_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create feds_employment_volunteer content'.
  $permissions['create feds_employment_volunteer content'] = array(
    'name' => 'create feds_employment_volunteer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'define view for terms in feds_employment_type'.
  $permissions['define view for terms in feds_employment_type'] = array(
    'name' => 'define view for terms in feds_employment_type',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'define view for vocabulary feds_employment_type'.
  $permissions['define view for vocabulary feds_employment_type'] = array(
    'name' => 'define view for vocabulary feds_employment_type',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'tvi',
  );

  // Exported permission: 'delete any feds_employment_volunteer content'.
  $permissions['delete any feds_employment_volunteer content'] = array(
    'name' => 'delete any feds_employment_volunteer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own feds_employment_volunteer content'.
  $permissions['delete own feds_employment_volunteer content'] = array(
    'name' => 'delete own feds_employment_volunteer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in feds_employment_type'.
  $permissions['delete terms in feds_employment_type'] = array(
    'name' => 'delete terms in feds_employment_type',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any feds_employment_volunteer content'.
  $permissions['edit any feds_employment_volunteer content'] = array(
    'name' => 'edit any feds_employment_volunteer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own feds_employment_volunteer content'.
  $permissions['edit own feds_employment_volunteer content'] = array(
    'name' => 'edit own feds_employment_volunteer content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in feds_employment_type'.
  $permissions['edit terms in feds_employment_type'] = array(
    'name' => 'edit terms in feds_employment_type',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter feds_employment_volunteer revision log entry'.
  $permissions['enter feds_employment_volunteer revision log entry'] = array(
    'name' => 'enter feds_employment_volunteer revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_employment_volunteer authored by option'.
  $permissions['override feds_employment_volunteer authored by option'] = array(
    'name' => 'override feds_employment_volunteer authored by option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_employment_volunteer authored on option'.
  $permissions['override feds_employment_volunteer authored on option'] = array(
    'name' => 'override feds_employment_volunteer authored on option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_employment_volunteer promote to front page option'.
  $permissions['override feds_employment_volunteer promote to front page option'] = array(
    'name' => 'override feds_employment_volunteer promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_employment_volunteer published option'.
  $permissions['override feds_employment_volunteer published option'] = array(
    'name' => 'override feds_employment_volunteer published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_employment_volunteer revision option'.
  $permissions['override feds_employment_volunteer revision option'] = array(
    'name' => 'override feds_employment_volunteer revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override feds_employment_volunteer sticky option'.
  $permissions['override feds_employment_volunteer sticky option'] = array(
    'name' => 'override feds_employment_volunteer sticky option',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search feds_employment_volunteer content'.
  $permissions['search feds_employment_volunteer content'] = array(
    'name' => 'search feds_employment_volunteer content',
    'roles' => array(
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
