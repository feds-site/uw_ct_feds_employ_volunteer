<?php

/**
 * @file
 * uw_ct_feds_employ_volunteer.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_feds_employ_volunteer_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: views_view
  $overrides["views_view.feds_employment_opportunities.display|feed_1"] = unserialize('O:13:"views_display":7:{s:15:"display_options";a:7:{s:5:"query";a:2:{s:4:"type";s:11:"views_query";s:7:"options";a:0:{}}s:5:"pager";a:1:{s:4:"type";s:4:"some";}s:12:"style_plugin";s:3:"rss";s:10:"row_plugin";s:8:"node_rss";s:8:"defaults";a:2:{s:13:"filter_groups";b:0;s:7:"filters";b:0;}s:7:"filters";a:2:{s:6:"status";a:6:{s:2:"id";s:6:"status";s:5:"table";s:4:"node";s:5:"field";s:6:"status";s:5:"value";i:1;s:5:"group";i:1;s:6:"expose";a:1:{s:8:"operator";b:0;}}s:4:"type";a:6:{s:2:"id";s:4:"type";s:5:"table";s:4:"node";s:5:"field";s:4:"type";s:5:"value";a:1:{s:25:"feds_employment_volunteer";s:25:"feds_employment_volunteer";}s:5:"group";i:1;s:6:"expose";a:4:{s:11:"operator_id";s:7:"type_op";s:5:"label";s:4:"Type";s:8:"operator";s:7:"type_op";s:10:"identifier";s:4:"type";}}}s:4:"path";s:18:"opportunities-feed";}s:8:"db_table";s:13:"views_display";s:3:"vid";i:0;s:2:"id";s:6:"feed_1";s:13:"display_title";s:18:"Opportunities Feed";s:14:"display_plugin";s:4:"feed";s:8:"position";i:0;}');

 return $overrides;
}
