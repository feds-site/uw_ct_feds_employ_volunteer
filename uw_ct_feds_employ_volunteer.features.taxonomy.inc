<?php

/**
 * @file
 * uw_ct_feds_employ_volunteer.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_ct_feds_employ_volunteer_taxonomy_default_vocabularies() {
  return array(
    'feds_employment_type' => array(
      'name' => 'Feds employment type',
      'machine_name' => 'feds_employment_type',
      'description' => 'Categories for filtering feds work opportunities.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
      'base_i18n_mode' => 0,
      'base_language' => 'und',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
