<?php

/**
 * @file
 * uw_ct_feds_employ_volunteer.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_feds_employ_volunteer_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_volunteer--job-opportunities:node/56.
  $menu_links['main-menu_volunteer--job-opportunities:node/56'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/56',
    'router_path' => 'node/%',
    'link_title' => 'Volunteer & Job Opportunities',
    'options' => array(
      'alter' => TRUE,
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'main-menu_volunteer--job-opportunities:node/56',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -33,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Volunteer & Job Opportunities');

  return $menu_links;
}
