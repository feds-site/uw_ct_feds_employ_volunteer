<?php

/**
 * @file
 * uw_ct_feds_employ_volunteer.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_ct_feds_employ_volunteer_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'additional_settings__active_tab_feds_employment_volunteer';
  $strongarm->value = 'edit-menu';
  $export['additional_settings__active_tab_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_feds_employment_volunteer';
  $strongarm->value = '0';
  $export['comment_anonymous_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['comment_default_mode_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_feds_employment_volunteer';
  $strongarm->value = '50';
  $export['comment_default_per_page_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_feds_employment_volunteer';
  $strongarm->value = '0';
  $export['comment_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['comment_form_location_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_feds_employment_volunteer';
  $strongarm->value = '1';
  $export['comment_preview_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['comment_subject_field_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'default_menu_link_enabled_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['default_menu_link_enabled_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_enable_revisions_page_node_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['diff_enable_revisions_page_node_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_show_preview_changes_node_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['diff_show_preview_changes_node_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'diff_view_mode_preview_node_feds_employment_volunteer';
  $strongarm->value = 'full';
  $export['diff_view_mode_preview_node_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_comment_filter_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['entity_translation_comment_filter_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_hide_translation_links_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['entity_translation_hide_translation_links_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_node_metadata_feds_employment_volunteer';
  $strongarm->value = '0';
  $export['entity_translation_node_metadata_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__feds_employment_volunteer';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'entity_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'embedded' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'ical' => array(
        'custom_settings' => FALSE,
      ),
      'diff_standard' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'locations' => array(
          'weight' => '5',
        ),
        'metatags' => array(
          'weight' => '7',
        ),
        'path' => array(
          'weight' => '3',
        ),
        'redirect' => array(
          'weight' => '6',
        ),
        'xmlsitemap' => array(
          'weight' => '4',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'forward_display_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['forward_display_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_feds_employment_volunteer';
  $strongarm->value = '0';
  $export['language_content_type_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'linkchecker_scan_comment_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['linkchecker_scan_comment_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'linkchecker_scan_node_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['linkchecker_scan_node_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_cancel_feds_employment_volunteer';
  $strongarm->value = '0';
  $export['mb_content_cancel_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_sac_feds_employment_volunteer';
  $strongarm->value = '0';
  $export['mb_content_sac_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'mb_content_tabcn_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['mb_content_tabcn_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_feds_employment_volunteer';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_feds_employment_volunteer';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_node__feds_employment_volunteer';
  $strongarm->value = TRUE;
  $export['metatag_enable_node__feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_taxonomy_term__feds_employment_type';
  $strongarm->value = TRUE;
  $export['metatag_enable_taxonomy_term__feds_employment_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_feds_employment_volunteer';
  $strongarm->value = array(
    0 => 'moderation',
    1 => 'revision',
  );
  $export['node_options_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_feds_employment_volunteer';
  $strongarm->value = '1';
  $export['node_preview_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_revision_delete_number_feds_employment_volunteer';
  $strongarm->value = '50';
  $export['node_revision_delete_number_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_revision_delete_track_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['node_revision_delete_track_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['node_submitted_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_title_type_feds_employment_volunteer';
  $strongarm->value = '';
  $export['page_title_type_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'page_title_type_feds_employment_volunteer_showfield';
  $strongarm->value = 0;
  $export['page_title_type_feds_employment_volunteer_showfield'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_feds_employment_volunteer_en_pattern';
  $strongarm->value = '';
  $export['pathauto_node_feds_employment_volunteer_en_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_feds_employment_volunteer_pattern';
  $strongarm->value = '/opportunities/[node:title]';
  $export['pathauto_node_feds_employment_volunteer_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_feds_employment_volunteer_und_pattern';
  $strongarm->value = '';
  $export['pathauto_node_feds_employment_volunteer_und_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_feds_employment_type_pattern';
  $strongarm->value = '';
  $export['pathauto_taxonomy_term_feds_employment_type_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'save_continue_feds_employment_volunteer';
  $strongarm->value = 'Save and add fields';
  $export['save_continue_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_expand_fieldset_feds_employment_volunteer';
  $strongarm->value = '0';
  $export['scheduler_expand_fieldset_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_enable_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['scheduler_publish_enable_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_moderation_state_feds_employment_volunteer';
  $strongarm->value = 'published';
  $export['scheduler_publish_moderation_state_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_past_date_feds_employment_volunteer';
  $strongarm->value = 'error';
  $export['scheduler_publish_past_date_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_required_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['scheduler_publish_required_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_revision_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['scheduler_publish_revision_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_publish_touch_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['scheduler_publish_touch_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_default_time_feds_employment_volunteer';
  $strongarm->value = '';
  $export['scheduler_unpublish_default_time_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_enable_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['scheduler_unpublish_enable_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_moderation_state_feds_employment_volunteer';
  $strongarm->value = 'draft';
  $export['scheduler_unpublish_moderation_state_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_required_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['scheduler_unpublish_required_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_unpublish_revision_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['scheduler_unpublish_revision_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'scheduler_use_vertical_tabs_feds_employment_volunteer';
  $strongarm->value = '1';
  $export['scheduler_use_vertical_tabs_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'uw_page_settings_node_node_type_feds_employment_volunteer';
  $strongarm->value = 1;
  $export['uw_page_settings_node_node_type_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'webform_node_feds_employment_volunteer';
  $strongarm->value = 0;
  $export['webform_node_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'workbench_moderation_default_state_feds_employment_volunteer';
  $strongarm->value = 'draft';
  $export['workbench_moderation_default_state_feds_employment_volunteer'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'xmlsitemap_settings_node_feds_employment_volunteer';
  $strongarm->value = array(
    'status' => '0',
    'priority' => '0.5',
  );
  $export['xmlsitemap_settings_node_feds_employment_volunteer'] = $strongarm;

  return $export;
}
